import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class Main {
	
	static String KEY_PATH = "key.txt";
	static String IV = "AAAAAAAA";

	
	public static void main(String[] args) {
		
		EncriptaDecriptaDES des = new EncriptaDecriptaDES(IV);
		String type = args[0];
		String fileName = args[1];
		String outFile = args[2];
		Calendar cal = Calendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
	    System.out.println( sdf.format(cal.getTime()) );
		switch (type) {
		case "-e":
			
			System.out.println("Start Encrypt: " + fileName +" - " + outFile + " " + new Date().getTime());
			try {
				des.encrypt(KEY_PATH, fileName, outFile);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("End Encrypt: " + fileName +" - " + outFile + " " + new Date().getTime());
			break;
		case "-d":
			try {
				des.decrypt(fileName, outFile);
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		}
		cal = Calendar.getInstance();
	    System.out.println( sdf.format(cal.getTime()) );
	}

}
