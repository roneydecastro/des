import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.Security;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class EncriptaDecriptaDES {
	
	String IV; 
	static byte[] encryptedText;
	byte[] buf = new byte[1024];
	static byte[] chaveencriptacaoBytes;


	
	public EncriptaDecriptaDES(String IV) { 
		this.IV = IV;
	} 
	
	/** Save the specified TripleDES SecretKey to the specified file */
	public static void writeKey(SecretKey key, File f) throws IOException,
	      NoSuchAlgorithmException, InvalidKeySpecException {
	    // Convert the secret key to an array of bytes like this
	    SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("DES");
	    DESKeySpec keyspec = (DESKeySpec) keyfactory.getKeySpec(key,DESKeySpec.class);
	    byte[] rawkey = keyspec.getKey();

	    // Write the raw key to the file
	    FileOutputStream out = new FileOutputStream(f);
	    out.write(rawkey);
	    out.close();
	  }
	
	public static void writeKey(String key, File f) throws IOException{
		   
		byte[] encoded = key.getBytes();
	    // Write the raw key to the file
	    FileOutputStream out = new FileOutputStream(f);
	    chaveencriptacaoBytes = encoded;
	    out.write(encoded);
	    out.close();
	  }
  
  public void encrypt(String filePathKey, String inFile, String outSendFile) 
		  throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IOException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException{
		String outFile = outSendFile;

	    File keyfile = new File(filePathKey);
	    File file = new File(inFile);
	    //byte fileContent[] = new byte[(int)file.length()];

	    OutputStream out = null;
		InputStream in = null;
		try {
			out = new FileOutputStream(outFile);
			in = new FileInputStream(file);
			//in.read(fileContent);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}  
		
		
	    Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
		writeKey(nextIntInRange(1,8), keyfile);

	    SecretKeySpec key = new SecretKeySpec(chaveencriptacaoBytes, "DES"); 		
	    //SecretKey key = KeyGenerator.getInstance("DES").generateKey();
	    

	    AlgorithmParameterSpec algParamSpec = new IvParameterSpec(IV.getBytes("UTF-8"));
	    Cipher m_encrypter = Cipher.getInstance("DES/CBC/PKCS5Padding");

	    m_encrypter.init(Cipher.ENCRYPT_MODE, key, algParamSpec);
	    
		encrypt(in,out, m_encrypter);
		
	    //byte[] encryptedText = m_encrypter.doFinal(fileContent);
	    
	    //out.write(encryptedText);
	    System.out.println("Encrypt Done"); 
	    in.close();
	    
	    /*File[] files = new File[2];
	  	files[0] = new File(filePathKey);
	  	files[1] = new File(outFile);
	  	File mergedFile = new File(outSendFile);
	  	mergeFiles(files, mergedFile);*/

  }
  
  public String nextIntInRange(int min, int max) {
		Random rng = new Random();
		String saida = "";
		for(int i=min;i<=max;i++){
			saida = saida.concat(""+rng.nextInt(9));
		}
		return saida;
}

  
  public void encrypt(InputStream in, OutputStream out, Cipher ecipher)
	{
		try
		{
			out = new CipherOutputStream(out, ecipher);
			int numRead = 0;
			while ((numRead = in.read(buf)) >= 0)
			{
				out.write(buf, 0, numRead);
			}
		    out.flush();
			out.close();
		}
		catch (java.io.IOException e)
		{
		}
	}
  
  
  	  public void decrypt(String inFile, String outFile) throws 
  	  			NoSuchAlgorithmException, NoSuchPaddingException, 
  	  			IllegalBlockSizeException, BadPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IOException{
  		 
  		 System.out.println("Decrypt Start");  
  		OutputStream out = null;
 		try {
 			out = new FileOutputStream(outFile);
 		} catch (FileNotFoundException e1) {
 			e1.printStackTrace();
 		}  
  		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
	    SecretKey key;
		key = getSecreteKey("key.txt");
		
	    // for CBC; must be 8 bytes
	    //byte[] initVector = new byte[] { 0x10, 0x10, 0x01, 0x04, 0x01, 0x01, 0x01, 0x02 };

	    AlgorithmParameterSpec algParamSpec = new IvParameterSpec(IV.getBytes("UTF-8"));
	    Cipher m_decrypter = Cipher.getInstance("DES/CBC/PKCS5Padding");

	    m_decrypter.init(Cipher.DECRYPT_MODE, key, algParamSpec);

	    InputStream in = new FileInputStream(inFile);
		decrypt(in,out,m_decrypter);
		
	    in.close();
	    //System.out.println(new String(decryptedText));
  		System.out.println("Decrypt Done");
	  }
  	  
  	  
  	public void decrypt(InputStream in, OutputStream out, Cipher dcipher)
	{
		try
		{
			in = new CipherInputStream(in, dcipher);
			int numRead = 0;
			while ((numRead = in.read(buf)) >= 0)
			{
				out.write(buf, 0, numRead);
			}
		    out.flush();
			out.close();
			
		}
		catch (java.io.IOException e)
		{
		}
	}
  	public SecretKey getSecreteKey(String filePath){
  		SecretKey key = null;
	    try {
	        Cipher c = Cipher.getInstance("DESede");
	    } catch (Exception e) {
	        System.err.println("Installing SunJCE provider.");
	        Provider sunjce = new com.sun.crypto.provider.SunJCE();
	        Security.addProvider(sunjce);
	    }

	  
	   //Delimiter used in CSV file
		try {
			File file = new File(filePath);
			BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
		
			byte[] buff=new byte[(int) file.length()];
			for (int i=0; i < file.length(); i++) {
			       buff[i]=(byte)bis.read();
			}
			
			
			/*DataInputStream in = new DataInputStream(new FileInputStream(f));
		    byte[] rawkey = new byte[(int) f.length()];
		    in.readFully(rawkey);
		    in.close();*/
			key = readKey(buff);

			} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			} catch (InvalidKeyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidKeySpecException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return key;
  	}
		public SecretKey readKey(byte[] rawkey) throws IOException,
		    NoSuchAlgorithmException, InvalidKeyException,
		    InvalidKeySpecException {
		  // Read the raw bytes from the keyfile
		  // Convert the raw bytes to a secret key like this
		  DESKeySpec keyspec = new DESKeySpec(rawkey);
		  SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("DES");
		  SecretKey key = keyfactory.generateSecret(keyspec);
		  return key;
		}

	  
	  public static void mergeFiles(File[] files, File mergedFile) {
			 
        OutputStream out = null;
			try {
				out = new FileOutputStream(mergedFile);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}

			for (File f : files) {
				System.out.println("merging: " + f.getName());
				FileInputStream fis;
				try {
					fis = new FileInputStream(f);
		            InputStream in = new FileInputStream(f);
	 
					byte[] buf = new byte[1024];
			        int len;
			        while ((len = in.read(buf)) > 0){
			                out.write(buf, 0, len);
			        }
			        in.close();
					
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	 
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	 
		}

}